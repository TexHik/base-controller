﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;
namespace IngameScript
{
    internal class ModuleLogInfo
    {
        public string Initiator { get; set; }
        public string Text { get; set; }
    }
    internal class ErrorController : Module
    {
        const int LOGS_SIZE = 50;
        const int ERRORS_SIZE = 10;
        Queue<ModuleLogInfo> systemErrors = new Queue<ModuleLogInfo>();
        Queue<ModuleLogInfo> baseErrors = new Queue<ModuleLogInfo>();

        Queue<ModuleLogInfo> logs = new Queue<ModuleLogInfo>();


        IMyTextPanel logPanel;
        IMyTextPanel errorPanel;

        bool Ready = false;
        public ErrorController(Program pr, string logPanelName, string errorPanelName) : base(pr)
        {
            logPanel = pr.GridTerminalSystem.GetBlockWithName(logPanelName) as IMyTextPanel;
            if (logPanel != null)
            {
                logPanel.TextPadding = 0;
                logPanel.FontSize = 0.3f;
                logPanel.Font = "Monospace";
            }
            errorPanel = pr.GridTerminalSystem.GetBlockWithName(errorPanelName) as IMyTextPanel;
            if (errorPanel != null)
            {
                errorPanel.TextPadding = 0;
                errorPanel.FontSize = 0.3f;
                errorPanel.Font = "Monospace";
            }
            if (errorPanel != null && logPanel != null)
            {
                Ready = true;
            }
        }

        public void ReportSystemError(string text, string initiatorModule)
        {
            var error = new ModuleLogInfo
            {
                Initiator = initiatorModule,
                Text = text
            };
            this.systemErrors.Enqueue(error);
            if (this.systemErrors.Count > ERRORS_SIZE)
            {
                this.systemErrors.Dequeue();
            }
        }
        public void ReportBaseError(string text, string initiatorModule)
        {
            var error = new ModuleLogInfo
            {
                Initiator = initiatorModule,
                Text = text
            };
            this.systemErrors.Enqueue(error);
            if (this.baseErrors.Count > ERRORS_SIZE)
            {
                this.baseErrors.Dequeue();
            }
        }
        public void Log(string text, string initiatorModule)
        {
            var error = new ModuleLogInfo
            {
                Initiator = initiatorModule,
                Text = text
            };
            this.logs.Enqueue(error);
            if (this.logs.Count > LOGS_SIZE)
            {
                this.logs.Dequeue();
            }
        }

        public override void doJob()
        {
            if (!Ready)
                return;
            StringBuilder builder = new StringBuilder(100);
            builder.Append("System errors:\n");
            foreach (var p in this.systemErrors) {
                builder.Append($"\t[{p.Initiator}] {p.Text}\n");
            }
            builder.Append('\n');
            builder.Append("Base errors:\n");
            foreach (var p in this.baseErrors)
            {
                builder.Append($"\t[{p.Initiator}] {p.Text}\n");
            }
            builder.Append('\n');
            this.errorPanel.WriteText(builder.ToString());


            builder = new StringBuilder(100);
            builder.Append("Base logs:\n");
            foreach (var p in this.logs)
            {
                builder.Append($"\t[{p.Initiator}] {p.Text}\n");
            }
            this.logPanel.WriteText(builder.ToString());
        }

    }
}
