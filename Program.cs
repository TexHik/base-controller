﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        Module autoCraft;
        Module resourseController;
        Module energyController;
        internal ErrorController errorController;
        internal RuntimeController runtimeController;

        int ticker = 0;
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
            errorController = new ErrorController(this, "[LogPanel]Logs", "[LogPanel]Errors");

            runtimeController = new RuntimeController(this, "[LogPanel]Runtime");

            autoCraft = new AutoCraftController(this, "[AutoCraft]Panel", "BaseMainAssembler");
            resourseController = new ResourсeController(this, "[BaseContainer]Components", "[BaseContainer]Resources", "[BaseContainer]Ores", "[BaseContainer]Tools");
            energyController = new EnergyController(this, "[Energy]Panel");
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means. 
            // 
            // This method is optional and can be removed if not
            // needed.
        }

        public void Main(string argument, UpdateType updateSource)
        {
            var start = DateTime.Now;
            if (ticker % 20 == 0)
            {
                if (runtimeController.tasks.Count == 0)
                {
                    if (ticker > 500) ticker = 0;
                } else
                {
                    runtimeController.doJob();
                }
                errorController.doJob();
            }
            switch (ticker)
            {
                case (100):
                    autoCraft.doJob();
                    break;
                case (200):
                    resourseController.doJob();
                    break;
                case (300):
                    energyController.doJob();
                    break;
            }
            Echo(ticker.ToString() + " " + (DateTime.Now - start).TotalMilliseconds.ToString());
            ticker++;
        }
    }
}
