﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    internal class EnergyController : Module
    {
        IMyTextPanel infoPanel;
        List<IMyReactor> reactors = new List<IMyReactor>();
        List<IMyBatteryBlock> batteries = new List<IMyBatteryBlock>();

        public EnergyController(Program pr, string panelName) : base(pr)
        {
            infoPanel = pr.GridTerminalSystem.GetBlockWithName(panelName) as IMyTextPanel;
            pr.GridTerminalSystem.GetBlocksOfType<IMyReactor>(reactors);
            pr.GridTerminalSystem.GetBlocksOfType<IMyBatteryBlock>(batteries);

            reactors = reactors.Where(p => (p as IMyTerminalBlock)?.IsSameConstructAs(pr.Me) ?? false).ToList();
            batteries = batteries.Where(p => (p as IMyTerminalBlock)?.IsSameConstructAs(pr.Me)??false).ToList();

            if (this.reactors.Count == 0)
                this.runtime.errorController.ReportBaseError("Reactors not found" ,"EnergyController");
            if (this.batteries.Count == 0)
                this.runtime.errorController.ReportBaseError("Batteries not found", "EnergyController");
            if (this.infoPanel == null)
                this.runtime.errorController.ReportBaseError("InfoPanel not found", "EnergyController");

            infoPanel.TextPadding = 0;
            infoPanel.FontSize = 0.5f;
            infoPanel.Font = "Monospace";
            this.runtime.errorController.Log("Initialized", "EnergyController");
        }
        public override void doJob()
        {
            float totalStored = 0;
            float maxStored = 0;
            batteries.ForEach(p=> { totalStored += p.CurrentStoredPower; maxStored += p.MaxStoredPower; });

            float totalOutput = 0;
            float maxOutput = 0;
            reactors.ForEach(p => { totalOutput += p.CurrentOutput; maxOutput += p.MaxOutput; });

            StringBuilder text = new StringBuilder();
            text.AppendLine($"Stored power: {Math.Round(totalStored, 2)} / {Math.Round(maxStored, 2)} MW/h");
            text.AppendLine($"Producing power: {Math.Round(totalOutput, 2)} / {Math.Round(maxOutput, 2)} MW");
            infoPanel.WriteText(text.ToString());
        }
    }
}
