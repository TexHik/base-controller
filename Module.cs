﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngameScript
{
    internal abstract class Module
    {
        protected Program runtime;

        public abstract void doJob();
        public Module(Program pr)
        {
            runtime = pr;
        }
    }
}
