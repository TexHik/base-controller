﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    internal class AutoCraftController : Module
    {
        private static readonly Dictionary<string, double> quota = new Dictionary<string, double>
        {
            {"SteelPlate" , 50000},
            {"InteriorPlate" , 50000},
            {"Motor" , 10000},
            {"MetalGrid" , 5000},
            {"Computer" , 10000},
            {"Construction" , 50000},
            {"SolarCell" , 0},
            {"PowerCell" , 5000},
            {"Display" , 1000},
            {"LargeTube" , 10000},
            {"SmallTube" , 10000},
            {"Girder" , 1000},
            {"BulletproofGlass" , 1000},
            {"Superconductor" , 100},
            {"RadioCommunication" , 100},
            {"Detector" , 100},
            {"Reactor" , 200},
            {"GravityGenerator" , 200},
            {"Thrust" , 0},
            {"ZoneChip" , 0},

            {"Explosives" , 0},
            //{ "SuperComputer", 0},
        };

        IMyTextPanel infoPanel;
        IMyAssembler assembler;
        List<IMyCargoContainer> containers = new List<IMyCargoContainer>();

        private Dictionary<string, double> items { 
            
            get {
                Dictionary<string, double> items_buffer = new Dictionary<string, double>();
                var buffer1 = new List<MyInventoryItem>();
                assembler.OutputInventory.GetItems(buffer1);
                buffer1.ForEach(b =>
                {
                    if (quota.ContainsKey(b.Type.SubtypeId))
                    {
                        if (items_buffer.ContainsKey(b.Type.SubtypeId))
                            items_buffer[b.Type.SubtypeId] = items_buffer[b.Type.SubtypeId] + (double)b.Amount;
                        else
                            items_buffer.Add(b.Type.SubtypeId, (double)b.Amount);
                    }
                });
                containers.ForEach(p => {
                    var buffer = new List<MyInventoryItem>(); 
                    p.GetInventory().GetItems(buffer);

                    buffer.ForEach(b =>
                    {
                        if (quota.ContainsKey(b.Type.SubtypeId))
                        {
                            if (items_buffer.ContainsKey(b.Type.SubtypeId))
                                items_buffer[b.Type.SubtypeId] = items_buffer[b.Type.SubtypeId] + (double)b.Amount;
                            else
                                items_buffer.Add(b.Type.SubtypeId, (double)b.Amount);
                        }
                    });
                });
                return items_buffer;
            }
        }
        private Dictionary<string, double> queue
        {

            get
            {
                Dictionary<string, double> queue_buffer = new Dictionary<string, double>();
                var buffer = new List<MyProductionItem>();

                assembler.GetQueue(buffer);
                Dictionary<string, double> items_buffer = new Dictionary<string, double>();
                buffer.ForEach(p => {
                    string key = quota.FirstOrDefault(k => p.BlueprintId.ToString().Contains(k.Key)).Key;
                    if (!string.IsNullOrEmpty(key))
                    {
                        if (queue_buffer.ContainsKey(key))
                            queue_buffer[key] = queue_buffer[key] + (double)p.Amount;
                        else
                            queue_buffer.Add(key, (double)p.Amount);
                    }
                });
                return queue_buffer;
            }
        }
        private Dictionary<string, double> deficit { 
            get {
                Dictionary<string, double> deficit_buffer = new Dictionary<string, double>();
                Dictionary<string, double> items = new Dictionary<string,double>(this.items);
                Dictionary<string, double> queue = new Dictionary<string, double>(this.queue);
                foreach (var item in quota)
                {
                    double req = item.Value;
                    if (items.ContainsKey(item.Key))
                        req -= items[item.Key];
                    if (queue.ContainsKey(item.Key))
                        req -= queue[item.Key];
                    if (req > 0)
                    {
                        req = Math.Ceiling(req);
                        deficit_buffer.Add(item.Key, req);
                    }
                }
                return deficit_buffer;
            }
        }


        public AutoCraftController(Program pr, string panelName, string assemblerName) : base(pr)
        {
            infoPanel = pr.GridTerminalSystem.GetBlockWithName(panelName) as IMyTextPanel;
            assembler = pr.GridTerminalSystem.GetBlockWithName(assemblerName) as IMyAssembler;
            pr.GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(containers);

            if (this.containers.Count == 0)
                this.runtime.errorController.ReportBaseError("Containers not found", "AutoCraftController");
            if (this.assembler == null)
                this.runtime.errorController.ReportBaseError("Assembler not found", "AutoCraftController");
            if (this.infoPanel == null)
                this.runtime.errorController.ReportBaseError("InfoPanel not found", "AutoCraftController");

            infoPanel.TextPadding = 0;
            infoPanel.FontSize = 0.5f;
            infoPanel.Font = "Monospace";
            this.runtime.errorController.Log("Initialized", "AutoCraftController");
        }
        public override void doJob()
        {
            //51 символ в строке
            //17 строк
            assembler.Enabled = true;
            infoPanel.Enabled = true;
            var deficit = new Dictionary<string, double>(this.deficit);
            var items = new Dictionary<string, double>(this.items);
            // Panel Drawing

            StringBuilder text = new StringBuilder();
            foreach (var item in AutoCraftController.quota)
            {
                if (item.Value == 0)
                    continue;

                double d;
                if (!items.TryGetValue(item.Key, out d))
                    d = 0;
                double percentage = Math.Min(1, d / item.Value);
                //d=how many i already have

                string row = $"[{MyExtension.ProgressBar(12, percentage)}] {item.Key}".PadRight(37);
                string info = $"{Math.Round(d)} / {Math.Round(item.Value)}";
                text.Append(row);
                text.Append(info);
                text.Append("\n");
            }
            infoPanel.WriteText(text);

            //Assembler requesting
            foreach (var item in deficit)
            {
                this.runtime.runtimeController.tasks.Enqueue(() =>
                {
                    bool success = false;
                    try
                    {
                        assembler.AddQueueItem(MyDefinitionId.Parse("MyObjectBuilder_BlueprintDefinition/" + item.Key), item.Value);
                        success = true;
                    }
                    catch
                    {
                        try
                        {
                            assembler.AddQueueItem(MyDefinitionId.Parse("MyObjectBuilder_BlueprintDefinition/" + item.Key + "Component"), item.Value);
                            success = true;
                        }
                        catch
                        {
                            try
                            {
                                assembler.AddQueueItem(MyDefinitionId.Parse("MyObjectBuilder_BlueprintDefinition/" + item.Key + "Magazine"), item.Value);
                                success = true;
                            }
                            catch (Exception e)
                            {
                                this.runtime.errorController.ReportSystemError($"Unable to add item {item.Key} to assembler queue", "AutoCraft");
                            }
                        }
                    }
                    if (success)
                    {
                        this.runtime.errorController.Log($"Item {item.Key} x{item.Value} queued to Assembler", "AutoCraftController");
                    }
                });
            }
        }
    }
}
