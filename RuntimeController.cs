﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    internal class RuntimeController : Module
    {
        const int AVERAGE_ON = 50;

        internal Queue<Action> tasks = new Queue<Action>();
        IMyTextPanel infoPanel;

        private DateTime lastExecution = DateTime.Now;

        Queue<double> AverageTPS = new Queue<double>();

        public RuntimeController(Program pr, string panelName) : base(pr)
        {
            infoPanel = pr.GridTerminalSystem.GetBlockWithName(panelName) as IMyTextPanel;
            if (infoPanel == null)
            {
                this.runtime.errorController.ReportBaseError("InfoPanel not found", "RuntimeController");
            }
        }

        public override void doJob()
        {
            this.tasks.Dequeue().Invoke();
            if (AverageTPS.Count > AVERAGE_ON)
            {
                AverageTPS.Dequeue();
            }
            AverageTPS.Enqueue(1 / (DateTime.Now - lastExecution).TotalSeconds);
            this.lastExecution = DateTime.Now;
            if (infoPanel != null)
            {
                var avg = AverageTPS.Sum() / AVERAGE_ON;
                var std = Math.Sqrt(AverageTPS.Sum(x => (x - avg) * (x - avg)) / AVERAGE_ON);
                infoPanel.WriteText($"Tasks per second: {Math.Round(avg,2)}±{Math.Round(std,2)}\nTasks amount: {tasks.Count}");
            }
        }
    }
}
