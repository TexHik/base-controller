﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngameScript
{
    public static class MyExtension
    {
        public static string ProgressBar(int totalLength, double progress)
        {

            StringBuilder sb = new StringBuilder();
            int filledRed, filledYellow, filledGreen;
            if (progress>0.3)
                filledRed = (int)Math.Floor(totalLength * 0.3);
            else
                filledRed = (int)Math.Floor(totalLength * progress);

            if (progress > 0.75)
                filledYellow = (int)Math.Floor(totalLength * 0.75);
            else
                filledYellow = (int)Math.Floor(totalLength * (progress-0.3));

            filledGreen = (int)Math.Floor(totalLength * (progress - 0.75));

            if (filledGreen < 0) filledGreen = 0;
            if (filledYellow < 0) filledYellow = 0;
            if (filledRed < 0) filledRed = 0;

            sb.Append('\uE035', filledRed);
            sb.Append('\uE038', filledYellow);
            sb.Append('\uE036', filledGreen);
            int pad = totalLength - filledGreen - filledYellow - filledRed + 3;
            if (pad>0)
                sb.Append(' ', pad);
            return sb.ToString();
        }
    }
}
