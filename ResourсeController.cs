﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    internal class ResourсeController : Module
    {
        List<IMyCargoContainer> componentContainers;
        Dictionary<string, List<IMyCargoContainer>> componentContainersMap = new Dictionary<string, List<IMyCargoContainer>>();


        List<IMyCargoContainer> resourceContainers;
        Dictionary<string, List<IMyCargoContainer>> resourceContainersMap = new Dictionary<string, List<IMyCargoContainer>>();

        List<IMyCargoContainer> oreContainers;
        Dictionary<string, List<IMyCargoContainer>> oreContainersMap = new Dictionary<string, List<IMyCargoContainer>>();


        IMyCargoContainer toolsContainer;
        List<IMyTerminalBlock> containers = new List<IMyTerminalBlock>();
        public ResourсeController(Program pr, string componentContainerPattern, string resourceContainerPattern, string oreContainerPattern, string toolsContainerName) : base(pr)
        {
            pr.GridTerminalSystem.GetBlocks(containers);
            containers.RemoveAll(p => !(
                p is IMyRefinery ||
                p is IMyAssembler ||
                p is IMyCargoContainer ||
                p is IMyCockpit ||
                p is IMyShipConnector
            ));
            containers.RemoveAll(p => !p.HasInventory);

            toolsContainer = pr.GridTerminalSystem.GetBlockWithName(toolsContainerName) as IMyCargoContainer;
            containers.Remove(toolsContainer);


            componentContainers = containers.Where(p => p.CustomName.Contains(componentContainerPattern)).Select(p => p as IMyCargoContainer).ToList();
            containers = containers.Except(componentContainers.Select(p => p as IMyTerminalBlock)).ToList();

            resourceContainers = containers.Where(p => p.CustomName.Contains(resourceContainerPattern)).Select(p => p as IMyCargoContainer).ToList();
            containers = containers.Except(resourceContainers.Select(p => p as IMyTerminalBlock)).ToList();

            oreContainers = containers.Where(p => p.CustomName.Contains(oreContainerPattern)).Select(p => p as IMyCargoContainer).ToList();
            containers = containers.Except(oreContainers.Select(p => p as IMyTerminalBlock)).ToList();

            this.runtime.errorController.Log($"Initialized {componentContainers.Count} components containers", "ResourсeController");
            this.runtime.errorController.Log($"Initialized {resourceContainers.Count} resources containers", "ResourсeController");
            this.runtime.errorController.Log($"Initialized {oreContainers.Count} ores containers", "ResourсeController");
        }
        public override void doJob()
        {
            List<IMyTerminalBlock> conts = new List<IMyTerminalBlock>(containers);
            conts.AddRange(componentContainers);
            conts.AddRange(resourceContainers);
            conts.AddRange(oreContainers);
            conts.Add(toolsContainer);

            conts.ForEach(p => {
                this.runtime.runtimeController.tasks.Enqueue(() =>
                {
                    if (p == null)
                    {
                        return;
                    }
                        var items = new List<MyInventoryItem>();
                        IMyInventory inventory;
                    if (p is IMyAssembler)
                    {
                        inventory = (p as IMyAssembler).OutputInventory;
                    }
                    else if (p is IMyRefinery)
                    {
                        inventory = (p as IMyRefinery).OutputInventory;
                    }
                    else
                    {
                        inventory = p.GetInventory();
                    }
                    if (inventory == null)
                    {
                        return;
                    }
                            inventory.GetItems(items);
                            items.ForEach(i =>
                            {
                                if (i == null)
                                {
                                    return;
                                }

                                var amount = i.Amount;
                                var volumeMult = i.Type.GetItemInfo().Volume;
                                var volume = volumeMult * amount;

                                var subtypeId = i.Type.SubtypeId; //Gold Ice etc...
                                try
                                {
                                    var targetContainers = new List<IMyCargoContainer>();
                                    switch (i.Type.TypeId)
                                    {
                                        case ("MyObjectBuilder_Component"):
                                            targetContainers = componentContainers;
                                            break;
                                        case ("MyObjectBuilder_Ingot"):
                                            targetContainers = resourceContainers;
                                            break;
                                        case ("MyObjectBuilder_Ore"):
                                            targetContainers = oreContainers;
                                            break;
                                        case ("MyObjectBuilder_PhysicalGunObject"):
                                        case ("MyObjectBuilder_ConsumableItem"):
                                        case ("MyObjectBuilder_AmmoMagazine"):
                                            if (toolsContainer != null)
                                            {
                                                targetContainers = new List<IMyCargoContainer> { toolsContainer };
                                            }
                                            break;
                                        default:
                                            this.runtime.errorController.ReportSystemError($"Unknown item typeId: {i.Type.TypeId} - {subtypeId}", "ResourсeController");
                                            return;
                                    }

                                    if (p is IMyCargoContainer)
                                    {
                                        if (targetContainers.Contains(p as IMyCargoContainer))
                                        {
                                            return;
                                        }
                                    }

                                    foreach (var target in targetContainers)
                                    {
                                        var freeVolumeAllowed = (target.GetInventory().MaxVolume - target.GetInventory().CurrentVolume);
                                        var freeAmountAllowed = new MyFixedPoint();
                                        freeAmountAllowed.RawValue = (long)(freeVolumeAllowed.RawValue / volumeMult); ;
                                        if (freeAmountAllowed > amount)
                                        {
                                            freeAmountAllowed = amount;
                                        }
                                        inventory.TransferItemTo(target.GetInventory(), i, freeAmountAllowed);
                                        amount -= freeAmountAllowed;
                                        if (amount <=0)
                                        {
                                            break;
                                        }
                                    }
                                    if (amount > 0)
                                    {
                                        this.runtime.errorController.ReportBaseError($"[OUT OF STORAGE] Failed to move item: {i.Type.TypeId}, Amount: {amount} Volume: {volume}", "ResourсeController");
                                    }

                                }
                                catch (Exception e)
                                {
                                    this.runtime.errorController.ReportSystemError($"Failed to move item: {i.Type.TypeId}: {e.Message}", "ResourсeController");
                                }
                            });
                            this.runtime.errorController.Log($"Sorted container {p.CustomName}", "ResourсeController");
                });
            });
        }
    }
}
